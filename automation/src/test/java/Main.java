import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Main {
    WebDriver driver;


    @BeforeClass
    public static void mainPrecondition() {
        System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\geckodriver\\geckodriver.exe");
    }


    @Before
    public void preconditions() {
        driver = new FirefoxDriver();
    }


    @Test
    public void todomvc() throws InterruptedException {
        driver.get("https://todomvc.com/examples/backbone/");
        WebElement input = driver.findElement(By.className("new-todo"));
        String[] values = {"one", "two", "three", "four", "five"};
        for (int i = 0; i < values.length; i++) {
            input.sendKeys(values[i], Keys.ENTER);
//        Thread.sleep(10000);
        }


        WebElement fieldSearch = driver.findElement(By.className("todo-count")); // один по css selector (у)
         System.out.println("todo-count" + input);

//        WebElement Search = driver.findElement(By.cssSelector("todo-list")); // один по css selector (у)
        List<WebElement> dva = driver.findElements(By.cssSelector("ul.todo-list li")); //много по тегу

        for (int i=0; i < dva.size(); i++) {
            String text = dva.get(i).getText();
            System.out.println("todo-list:" + text);
            String result = "OK";
            if (!text.equals(values[i])) {
                result = "FAIL";
            }
            System.out.println(result);
        }



      /*  String currentUrl = driver.getCurrentUrl();
        System.out.println(currentUrl);
        String negativeToUrl*/
    }

    @After
    public void postCondition(){
        driver.quit();
    }
}

